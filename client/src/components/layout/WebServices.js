import React from "react";
import {Link, withRouter} from 'react-router-dom';
import Footer from '../Footers/Footer.js';
import '../../assets/css/Services.css';
import '../../assets/css/webServices.css';
import ScrollAnimation from 'react-animate-on-scroll';

// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardBody,
  Container,
  Row,
  Col
} from "reactstrap";

class WebServices extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			serviceLevel: "Basic"
		}
		this.handleClick = this.handleClick.bind(this);
	}
	
	handleClick(evt){
		evt.preventDefault();
		this.setState({serviceLevel: evt.target.name})
		this.props.history.push({
			pathname: '/contact',
			state: {service: evt.target.name, pay: true}
		});
	}
	
	

  render() {
	  const projects = [
		  {name: 'CurlyAdmin.com', link: "https://www.curlyadmin.com", img: "curlyadmin_frontpage.png"},
		  {name: 'TaqueriaLos...', link: "https://taqueria-restaurant.herokuapp.com/", img: "taqueria_thumb1.png"},
		  {name: 'CoderChat', link: "https://coderchat-c0a8c.web.app/", img: "coderchat_chat.png"},
		  {name: 'RockyCoder.com', link: "https://www.rockycoder.com", img: "rockyportfolio_frontpage.png"},
	  ]
	  
	  const starterPage = [
		'4 Pages',
		'Contact Form',
		'Mobile Responsive',
		'Social Media Plugins',
		'Domain Name 1 year',
		'Free hosting 1 year',
	  ]
	  
	  const standardPage = [
		'10 Pages',
		'Contact Form',
		'Mobile Responsive',
		'Social Media Plugins',
		'Domain Name 1 year',
		'Free hosting 1 year',
		'CMS Admin panel',
		'SEO Ready',
		'Accept Payments'
	  ]
	  
	  const advancedPage = [
		'15+ Pages',
		'Contact Form',
		'Mobile Responsive',
		'Social Media Plugins',
		'Domain Name 1 year',
		'Free hosting 1 year',
		'CMS Admin panel',
		'SEO Ready',
		'Accept Payments',
		'E-Commerce Shop',
		'Resturant Order Plugin',
		'Reservation plugin'
	  ]
	  
    return (
      <>
        <main ref="main" className="services-background">
          <div className="services-background" style={{height: "80px"}}>
            <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
          </div>
          <section className="mt-4 pt-lg-5 services-background">
            <Container>
				<div className="justify-content-center text-center mb-lg-1 row">
					<div className="col-lg-10">
					
						<h2 className="font-weight-bold mb-4 text-color-default services-header-h2">Web Service</h2>
						
						<ScrollAnimation animateIn="fadeIn">
						<p className="text-muted lead-text">
							Website packages include customized development with minimal templating. Development is done using modern frameworks such as ReactJS for optimal speed. Each package can be customized, please contact us for consultation on your next project. 
						</p>
						</ScrollAnimation>
					</div>
				</div>
				
				
			{/* PRICING CARDS  */}
			<ScrollAnimation animateIn="fadeInUp">
              <Row className="justify-content-center mt-4 services-cards mb-5 pb-5">
                <Col lg="12">
                  <Row className="row-grid">
                    <Col lg="4">
                      <Card className="card-up-hover card-margin-bottom shadow border-0 text-center">
                        <CardBody className="py-3">
                          <h3 className="text-color-primary Services-card-h3 text-uppercase">
                        	WEB STARTER
                          </h3>
                          <div className="Services-description">
							  <div className="Services-task-amount">$500</div>
                          </div>
							<hr className="my-1 mb-1" />
							<div className="card-text-description">
								<div className="card-paragraph-description">
									<div className="description mt-3">
										<ul className="list-unstyled">
											{starterPage.map(item =>(
												<li><i className="fas fa-angle-right card-list-bullet" />{item}</li>
											))}
										</ul>
									</div>
								</div>
							</div>
							<div className="card-text-description">
								<div className="card-paragraph-description">
									<div className="web-description mt-3">
										*After one year:
										<ul className="list-unstyled">
											<li>Hosting $84 per year/ $7 month</li>
											<li>Domain name $12 per year</li>
											<li>Monthly Maintenance $30</li>
										</ul>
									</div>
								</div>
							</div>
                          <Button
                            className="mt-1 
									   shadow-sm border-0 
									   button-color-primary 
									   btn-rollover-color-primary 
									   card-button-text"
							  to="/contact"
							  tag={Link}
							  name="Basic"
							  onClick={this.handleClick}
                          >
							  	SELECT
                          </Button>
                        </CardBody>
                      </Card>
                    </Col>
                   <Col lg="4">
                      <Card className="card-up-hover card-margin-bottom shadow border-0 text-center">
                        <CardBody className="py-3">
                          <h3 className="text-color-danger Services-card-h3 text-uppercase">
                        		WEB STANDARD
                          </h3>
                          <div className="description Services-description">
							  <div className="Services-task-amount">$800</div>
                          </div>
							<hr className="my-1 mb-1" />
							<div className="card-text-description">
								<div className="card-paragraph-description">
									<div className="description mt-3">
										<ul className="list-unstyled">
											{standardPage.map(item =>(
												<li><i className="fas fa-angle-right card-list-bullet" />{item}</li>
											))}
										</ul>
									</div>
								</div>
							</div>
							<div className="card-text-description">
								<div className="card-paragraph-description">
									<div className="web-description mt-3">
										*After one year:
										<ul className="list-unstyled">
											<li>Hosting $84 per year/ $7 month</li>
											<li>Domain name $12 per year</li>
											<li>Monthly Maintenance $60</li>
										</ul>
									</div>
								</div>
							</div>
                          <Button
                            className="mt-1  
									   shadow-sm border-0 
									   button-color-danger 
									   btn-rollover-color-danger
									   card-button-text"
							  to="/contact"
							  tag={Link}
							  name="Super"
							  onClick={this.handleClick}
                          >
							  	SELECT
                          </Button>
                        </CardBody>
                      </Card>
                    </Col>
                    <Col lg="4">
                      <Card className="card-up-hover shadow border-0 text-center">
                        <CardBody className="py-3">
                          <h3 className="text-color-success Services-card-h3 text-uppercase">
                        	 WEB ADVANCED
                          </h3>
                          <div className="description Services-description">
							  <div className="Services-task-amount">$2000</div>
                          </div>
							<hr className="my-1 mb-1" />
							<div className="card-text-description">
								<div className="card-paragraph-description">
									<div className="description mt-3">
										<ul className="list-unstyled">
											{advancedPage.map(item =>(
												<li><i className="fas fa-angle-right card-list-bullet" />{item}</li>
											))}
										</ul>
									</div>
								</div>
							</div>
							<div className="card-text-description">
								<div className="card-paragraph-description">
									<div className="web-description mt-3">
										*After one year:
										<ul className="list-unstyled">
											<li>Hosting $84 per year/ $7 month</li>
											<li>Domain name $12 per year</li>
											<li>Monthly Maintenance $90</li>
										</ul>
									</div>
								</div>
							</div>
                          <Button
                            className="mt-1 
									   shadow-sm border-0 
									   button-color-success 
									   btn-rollover-color-success
									   card-button-text"
							  to="/contact"
							  tag={Link}
							  name="Curly"
							  onClick={this.handleClick}
                          >
							  	SELECT
                          </Button>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </Col>
              </Row>
			  </ScrollAnimation>
            </Container>
          </section>
		  <section className="webservices-project-section">
			<div className="web-projects-container">
				<div className="web-projects-header">
					<h2 className="font-weight-bold mb-4 text-color-default services-header-h2">Web Projects</h2>
				</div>
				<div className="web-projects-body">
					<div className="web-projects-body-container">
						{
							projects.map(project =>(
								<ScrollAnimation animateIn="fadeIn">
									<div className="web-project-item">
											<img 
												className="web-project-item-image" 
												src={require(`../../assets/images/${project.img}`)} 
												alt="Web Thumb" />
											<div className="web-project-overlay">
												<div className="web-project-item-text">
													
													<Link 
														to={{ pathname: `${project.link}` }}
														target="_blank"
														className="web-project-link"
														>
														{project.name}
														
													</Link>
												</div>
											</div>
									</div>
								</ScrollAnimation>
								)
							)
						}
						
						
					</div>
				</div>
			</div>			  
		  </section>
        </main>
		<Footer />
      </>
    );
  }
}

export default withRouter(WebServices);
